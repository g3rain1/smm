#Search MechMarket (smm) 
#Searches for key words on r/mechmarket and sends emails alerts

import email
import requests
import datetime
from dateutil import parser
import pytz
from lxml import html
from time import sleep
import smtplib
import getpass
import os
from collections import deque

search_for = []
flair_filter = []
from_email = ''
smtp_server = ''
to_email = ''
to_email_lst = []
password = ''

dirname = os.path.dirname(__file__)
fname = os.path.join(dirname, 'smm_links.txt')
cfgfile = os.path.join(dirname, 'smm.cfg')
base_url = 'https://old.reddit.com'
base_sub = '/r/mechmarket/new/'
repeat_period = 30 #seconds

# if not os.path.isfile(fname):
#     with open(fname, 'a+'):
#         os.utime(fname, None)

while True:
    #read values from config file
    with open(cfgfile, 'r') as fo:
        cfg = {}
        for line in fo:
            line = line.split('#',1)[0] # remove comments from config file
            item = line.split('=')
            data = item[1].strip().split(',')
            cfg[item[0]] = data

    search_for = cfg['terms']
    flair_filter = cfg['flair']
    flair_filter.append('') #sometimes a post is made without a flair so appending an empty string to the filter ensures we search those posts as well
    from_email = cfg['from'][0]
    to_email = ','.join(cfg['to'])
    to_email_lst = cfg['to']
    smtp_server = cfg['smtp_server'][0]
    if password == '' or password is None:
        password = cfg['email_pass'][0]

    if password == '' or password is None:
        password = getpass.getpass('Login ' + from_email + ':')
    
    #get a list of previously found links so we don't grab them again
    with open(fname, 'a+') as f:
        previous_links = f.readlines()
    previous_links = deque([x.strip() for x in previous_links])
    print(previous_links)

    #download the page
    try:
        page = requests.get(base_url + base_sub, headers = {'User-agent': 'Mozilla/5.0'})
    except Exception as e:
        print str(e)
        sleep(repeat_period)
        continue

    #scrub the html for links, this part is likely to break if reddit changes their layout again.
    dom = html.fromstring(page.text)
    links = dom.find_class('title may-blank')
    hrefs = []
    for link in links:
        href = link.get('href')
        print(href)
        #match terms
        if any (term in link.text.lower() for term in search_for) and href not in previous_links:
            timestamp =  parser.parse(link.getparent().getparent().find_class('live-timestamp')[0].get('datetime')).astimezone(pytz.timezone('US/Mountain'))
            flair = link.getparent().find_class('linkflairlabel')
            if len(flair) > 0:
                flair = flair[0].get('title')
            else:
                flair = 'None'
            #match flair
            if len(flair_filter) == 0 or flair in flair_filter:
                hrefs.append((flair + ' ' + link.text, base_url + href,timestamp))
                previous_links.appendleft(href)

    #if we found more than 0 links we will send out an email. 
    if len(hrefs) > 0:
        message_body = ''
        #compile found links into an email body
        for href in hrefs:
            print href[0] + '\n' + href[1] + '\n' + str(href[2]) + '\n\n'
            message_body = message_body + href[0] + '\r\n\r\n' + href[1] + '\r\n\r\n' + str(href[2]) + '\r\n\r\n\r\n\r\n'

        message = email.message_from_string(message_body)
        message['From'] = from_email
        message['To'] = to_email
        message['Subject'] = 'Found on r/mechmarket'
        print(password)
        mailer = smtplib.SMTP(smtp_server)
        mailer.ehlo()
        mailer.starttls()
        mailer.login(from_email, password)
        mailer.sendmail(from_email, to_email_lst, message.as_string())

    #re-write the previous links file
    if len(previous_links) > 25:
        for i in range(len(previous_links) - 25):
            previous_links.pop()
    with open(fname, 'w+') as f:
        for item in previous_links:
            f.write(item + '\n')

    sleep(repeat_period)
